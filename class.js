class Character{

    constructor(name, role){
        this.name=name;
        this.role=role;
    }
    
    armor(){
        console.log(`${this.name} is ${this.role} and has ${Math.random() * 100} armors`);
    }

    damage(){
        this.role === "mage" || this.role === "support"
        ? console.log("magic damage")
        : console.log("attack damage");
    }
}

class Mage extends Character{
    constructor(name){
        super(name,'mage')
    }
    magicPowers(){
        console.log('Daamn, I can fly!');
    }
}
class Support extends Mage{
heal = 0;
constructor(name,heal){
    super(name,'support')
    this.heal = heal;
}
healing(heal){
    console.log(` ${this.name} who is ${this.role} can heal ${heal}`);
}
}
class Adc extends Character{
    attackdamages = 0;
    constructor(name,attackdamages){
        super(name, 'adc');
        this.attackdamages = attackdamages;
    }
    range(){
         this.attackdamages < 30 ? console.log(' close ') : console.log('ranged');
    }
}

let nini = new Character("nini", "assasin");
let lily = new Mage("Lily");
let knight = new Support("Knight",10);
let wizzy = new Adc("wizzy",31);

console.log(nini);
console.log(lily);
console.log(knight);
console.log(wizzy);

nini.armor();
nini.damage();
knight.healing();